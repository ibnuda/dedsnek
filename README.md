# DedSnek

Got an inspiration for a keyboard design when a snake sneaked into my room
(though I killed it because of security reason).

The goal is a bare keyboard which its column can be moved upside down and
left right. So yeah, something like a snake.

Thumbcluster? Perhaps will use something similar to Redox's thumbcluster
but with 1.5u (not 2u). I fucked up my right thumb, yo!

Oh yeah, the plan is I will use laser cut 1.5mm stainless steel (304).
So the switches will fit perfectly, I hope.

## Current Work

![Current Progress](img/dedsnek.png)