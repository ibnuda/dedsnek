$fn = 200;

// ukuran satu tombol.
ukuran_tombol = 19;
setengah_tombol = ukuran_tombol / 2;
seperempat_tombol = setengah_tombol / 2;
seperlapan_tombol = seperempat_tombol / 2;

// as bagian bawah.
panjang_as = 120;
lubang_as = panjang_as + 10;

// tebal material.
selisih = 1.5;

diameter_sekrup = 1.5;

module segilima(x = 5, y = 5, m = 2.5) {
    selisih = x - m;
    nol = 0;
    polygon(points=[[nol, nol], [selisih, nol], [x, m], [x, y], [nol, y]]);
}

module tekukan_v2() {
    translate([setengah_tombol - 2.5, 0, 0]) {
        polygon(points=[[0, 0], [2.5, 2.5], [2.5, -2.5]]);
    }
    translate([-(setengah_tombol - 2.5), 0, 0]) {
        polygon(points=[[0, 0], [-2.5, 2.5], [-2.5, -2.5]]);
    }
}

module segienam (x, y, m) {
    union () {
        segilima(x, y, m);
        mirror([1, 0, 0]) {
            segilima(x, y, m);
        }
    }
}

module tekukan(lebar, tinggi, sisi_potongan) {
    x = lebar / 2;
    y = tinggi / 2;
    union() {
        segienam(x, y, sisi_potongan);
        mirror([0, 1, 0]) {
            segienam(x, y, sisi_potongan);
        }
    }
}

module unit_tombol() {
    square(size=[ukuran_tombol, ukuran_tombol], center=true);
}

module lubang_saklar() {
    ukuran_lubang = 13.97;
    square([ukuran_lubang, ukuran_lubang], center = true);
}

module satu_tombol() {
    difference() {
        unit_tombol();
        lubang_saklar();
    };
};

module kolom_saklar(jumlah_saklar) {
    for (i = [0 : (jumlah_saklar - 1)]) {
        translate([0, i * ukuran_tombol, 0]) {
            satu_tombol();
        }
    }
}

module kolom_rangka(jumlah_saklar) {
    for (i = [0 : (jumlah_saklar - 1)]) {
        translate([0, i * ukuran_tombol, 0]) {
            unit_tombol();
        }
    }
}

module tempat_sekrup(diameter) {
    difference() {
        circle(d = ukuran_tombol);
        translate([0, ukuran_tombol / 2, 0]) {
            square([ukuran_tombol, ukuran_tombol], center = true);
        }
        translate([0, - ukuran_tombol / 4, 0]) {
            circle(d = diameter);
        }
    } 
}

module plat_atas(jumlah_saklar) {
    union() {
        translate([0, jumlah_saklar * ukuran_tombol, 0]) {
            mirror([0, 1, 0]) {
                tempat_sekrup(diameter_sekrup);
            }
        }
        translate([0, (jumlah_saklar * ukuran_tombol) - seperempat_tombol, 0]) {
            tekukan(ukuran_tombol, setengah_tombol, 2.5);
        }
        kolom_saklar(jumlah_saklar);
        translate([0, -(setengah_tombol * 1.5), 0]) {
            tekukan(ukuran_tombol, setengah_tombol, 2.5);
        }
        translate([0, -ukuran_tombol, 0]) {
            tempat_sekrup(diameter_sekrup);
        }
    }
}

module rangka_bawah(jumlah_saklar) {
    setengah_selisih = selisih / 2;
    difference() {
        union() {
            translate([0, jumlah_saklar * ukuran_tombol + ukuran_tombol, 0]) {
                mirror([0, 1, 0]) {
                    tempat_sekrup(diameter_sekrup);
                }
            }
            translate([0, jumlah_saklar * ukuran_tombol + setengah_tombol, 0]) {
                square(size=[ukuran_tombol, ukuran_tombol], center = true);
            }
            translate([0, jumlah_saklar * ukuran_tombol - seperempat_tombol + selisih, 0]) {
                tekukan(ukuran_tombol, setengah_tombol, 2.5);
            }
            translate([0, jumlah_saklar * ukuran_tombol - setengah_tombol + setengah_selisih, 0]) {
                square(size=[ukuran_tombol, selisih], center = true);
            }
            kolom_rangka(jumlah_saklar);
            translate([0, 0 - setengah_tombol - setengah_selisih, 0]) {
                square(size=[ukuran_tombol, selisih], center = true);
            }
            translate([0, 0 - setengah_tombol - (setengah_tombol / 2) - setengah_selisih, 0]) {
                tekukan(ukuran_tombol, setengah_tombol, 2.5);
            }
            translate([0, 0 - (ukuran_tombol * 1.5) , 0]) {
                square(size=[ukuran_tombol, ukuran_tombol], center = true);
            }
            translate([0, -ukuran_tombol - ukuran_tombol, 0]) {
                tempat_sekrup(diameter_sekrup);
            }
        }
        translate([0, (jumlah_saklar / 2 - 0.5) * ukuran_tombol, 0]) {
            square(size=[diameter_sekrup, (jumlah_saklar + 3.5) * ukuran_tombol], center=true);
        }
    }
}

module as_utama(panjang, panjang_lubang) {
    separuh_as = panjang / 2;
    difference() {
        union() {
            square(size=[panjang, ukuran_tombol], center = true);
            translate([separuh_as, 0, 0]) {
                rotate([0, 0, 90]) {
                    tempat_sekrup(diameter_sekrup);
                }
            }
            translate([-separuh_as, 0, 0]) {
                rotate([0, 0, -90]) {
                    tempat_sekrup(diameter_sekrup);
                }
            }
        }
        square(size=[panjang_lubang, diameter_sekrup], center=true);
    }
}

module as_jempol() {
    rotate([0, 0, 90]) {
        as_utama(ukuran_tombol * 4, ukuran_tombol * 4 + 10);
    }
}

module satu_setengah_tombol() {
    difference() {
        square(size=[ukuran_tombol, ukuran_tombol * 1.5], center=true);
        lubang_saklar();
    }
}

module dua_setengah_tombol() {
    union() {
        satu_setengah_tombol();
        translate([0, (ukuran_tombol * 1.5 / 2) + (ukuran_tombol / 2), 0]) {
            satu_tombol();
        }
    }
}

module tempat_sekrup_jempol(jumlah_kolom) {
    difference() {
        hull() {
            translate([ukuran_tombol * jumlah_kolom / 4, 0, 0]) {
                circle(ukuran_tombol / 2);
            }
            translate([-ukuran_tombol * jumlah_kolom / 4, 0, 0]) {
                circle(ukuran_tombol / 2);
            }
        }
        translate([0, - ukuran_tombol / 2, 0]) {
            square(size=[ukuran_tombol * jumlah_kolom, ukuran_tombol], center=true);
        }
        translate([0, ukuran_tombol / 4, 0]) {
            hull() {
                translate([- (ukuran_tombol * jumlah_kolom * 0.3), 0]) {
                    circle(d = diameter_sekrup);
                }
                translate([ukuran_tombol * jumlah_kolom * 0.3, 0, 0]) {
                    circle(d = diameter_sekrup);
                }
            }
        }
    }
}
module kluster_jempol(jumlah_kolom) {
    union() {
        translate([setengah_tombol, (ukuran_tombol * 1.25 / 2) + (8 * ukuran_tombol / 4), 0]) {
            tempat_sekrup_jempol(jumlah_kolom);
        }
        translate([setengah_tombol, (ukuran_tombol * 1.25 / 2) + (3 * ukuran_tombol / 2), 0]) {
            tekukan(ukuran_tombol * jumlah_kolom, ukuran_tombol, seperempat_tombol);
        }
        for (i = [0 : (jumlah_kolom - 1)]) {
            translate([i * ukuran_tombol, 0, 0]) {
                dua_setengah_tombol();
            }
        }
        translate([setengah_tombol, - ukuran_tombol, 0]) {
            tekukan(ukuran_tombol * jumlah_kolom, ukuran_tombol, seperempat_tombol);
        }
        translate([setengah_tombol, - (6 * ukuran_tombol / 4), 0]) {
            mirror([0, 1, 0]) {
                tempat_sekrup_jempol(jumlah_kolom);
            }
        }
        
    }
}

module set_plat_kanan() {
    // telunjuk kiri
    translate([10, 10, 0]) {
        plat_atas(3);
    }

    // telunjuk kanan
    translate([31, 10, 0]) {
        plat_atas(3);
    }

    // tengah
    translate([52, 0, 0]) {
        plat_atas(4);
    }

    // manis
    translate([73, 0, 0]) {
        plat_atas(4);
    }

    // kelingking
    translate([94, -5, 0]) {
        plat_atas(3);
    }
}

module set_rangka_kanan() {
    // telunjuk kiri
    translate([10, 10, 0]) {
        rangka_bawah(3);
    }

    // telunjuk kanan
    translate([31, 10, 0]) {
        rangka_bawah(3);
    }

    // tengah
    translate([52, 0, 0]) {
        rangka_bawah(4);
    }

    // manis
    translate([73, 0, 0]) {
        rangka_bawah(4);
    }

    // kelingking
    translate([94, -5, 0]) {
        rangka_bawah(3);
    }
}
module sebelah_kanan() {
    translate([0, 0, 5]) {
        set_plat_kanan();
    }

    translate([40, 0, -1.5]) {
        as_utama(panjang_as, lubang_as);
    }

    translate([40, 25, -1.5]) {
        as_utama(panjang_as, lubang_as);
    }

    translate([0, 0, -3]) {
        set_rangka_kanan();
    }

    translate([40, 0, -4.5]) {
        as_utama(panjang_as, lubang_as);
    }

    translate([40, 25, -4.5]) {
        as_utama(panjang_as, lubang_as);
    }
    translate([-20, -5, -3]) {
        as_jempol();
    }
}
module segilima(x = 5, y = 5, m = 2.5) {
    selisih = x - m;
    nol = 0;
    polygon(points=[[nol, nol], [selisih, nol], [x, m], [x, y], [nol, y]]);
}

module rangka_jempol() {
    seperlima_tombol = ukuran_tombol / 5;
    panjang_as_jempol =  2.5 * ukuran_tombol * 3;
    dua_setengah_tombol = 2.5 * ukuran_tombol;
    difference() {
        square(size=[ukuran_tombol, panjang_as_jempol], center=true);
        hull() {
            translate([seperlima_tombol, panjang_as_jempol / 2 - 5, 0]) {
                circle(d = diameter_sekrup);
            }
            translate([seperlima_tombol, - (panjang_as_jempol / 2 - 5), 0]) {
                circle(d = diameter_sekrup);
            }
        }
        hull() {
            translate([- seperlima_tombol, panjang_as_jempol / 2 - 5, 0]) {
                circle(d = diameter_sekrup);
            }
            translate([- seperlima_tombol, - (panjang_as_jempol / 2 - 5), 0]) {
                circle(d = diameter_sekrup);
            }
        }
        translate([0, ukuran_tombol * 1.6, 0]) {
            tekukan_v2();
        }
        translate([0, - ukuran_tombol * 1.6, 0]) {
            tekukan_v2();
        }
    }
}

sebelah_kanan();
translate([-10, -50, 0]) {
    rotate([0, 0, 90]) {
        kluster_jempol(2);
    }
}
translate([-20, -50, -5]) {
    rotate([0, 0, 90]) {
        rangka_jempol();
    }
}